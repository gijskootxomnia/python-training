## Program week 2

1. Review homework
2. Review lists and loops

## Looking up

* `dict` and `tuple`; counting with a `dict`; sorting with a `dict`; `str.split`
* Exercise: list the five most common words in a document

## If nothing else

* `if` statement (`else`, `then`)
* Exercise : fizzbuzz
* Parsing dates with an external library
* Exercise: count the number of births on each weekday from a datafile

## Homework

* Repeat the exercises done today
* Use `pandas` to read the file as a `csv` file
* Use a `groupby` statement to calculate the average number of births in each month. What's your conclusion?
* Create a plot of the number of births each day
