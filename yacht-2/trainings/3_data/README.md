## Program week 3

### Review Homework exercise

### Series and DataFrames

* Loading data
* Adding a new column

## Do something

* Group means
* Value counts
* Plotting

### Homework - based on a random sample of 10000 cars in `rdw.xlsx`

* Create a plot of the number of cars sold per year. Include titles, good labels and pick a good size. Save it to your desktop as a `png` file.
* Which car brands are heavy for their price?
* Does the frequency of first letters of a car signs ("Kenteken") change over the years? For example, are car signs starting with a 'B' roughly spread evenly over all the years for which you have data? Hint: _Use the `.str.get(0)` function on the Kenteken column to get a count of all used first letters._
* (**) Do a data analysis
