import os

fd = "./homework/txt"

filenames = sorted(os.listdir(fd))

for fn in filenames:

    with open(os.path.join(fd, fn)) as f:

        txt = f.read()
        print(txt[0] + txt[-1], end = "")
