import string
import os
import random

sentence = "We are not meant to know everything, Mae. Did you ever think that perhaps our minds are delicately calibrated between the known and the unknown? That our souls need the mysteries of night and the clarity of day? Young people are creating ever-present daylight, and I think it will burn us all alive. There will be no time to reflect, to sleep to cool."
fd = "./homework"

chars = string.ascii_letters + ":;, !"
n = len(sentence) // 2

for i in range(n):

    start, end = sentence[2 * i], sentence[2 * i + 1]
    num_chars = random.randint(100, 200)
    random_chars = random.choices(chars, k = num_chars)

    filename = os.path.join(fd, "txt", "part_{:04}.txt".format(i))

    with open(filename, "w") as f:
        f.write(start + "".join(random_chars) + end)

quote = "The path of the righteous man is beset on all sides by the inequities of the selfish and the tyranny of evil men. Blessed is he who, in the name of charity and good will, shepherds the weak through the valley of the darkness, for he is truly his brother’s keeper and the finder of lost children. And I will strike down upon thee with great vengeance and furious anger those who attempt to poison and destroy My brothers. And you will know I am the Lord when I lay My vengeance upon you."[::-1]


with open(os.path.join(fd, "reversed_text.txt"), "w") as f:
    f.write(quote)
