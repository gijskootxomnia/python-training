
* Alle trainingen 17:30-20:30, eten om 17:00

| Wanneer | Onderwerp | Zelf doen |
| --- | --- | --- |
| voorbereiding | Wat is python; wat is programmeren | Bekijk en doe de opdrachten van Python Basics en Python Lists van Intro to Python for Data Science op https://www.datacamp.com/; lees de eerste drie hoofdstukken van de officiële python tutorial op https://docs.python.org/3/tutorial/, tot en met "3. An informal Introduction to Python"; installeer `python3.6`, bijvoorbeeld met https://www.anaconda.com/distribution/; schrijf je eerste script op basis van https://www.digitalocean.com/community/tutorials/how-to-write-your-first-python-3-program |
| training 8 februari | Programmeren, basics en theorie; variabelen, loops en condities | Scripts schrijven |
| training 22 februari | Libraries gebruiken; loops en functies | Scripts schrijven |
| training 15 maart  | Werken met tekstbestanden, structuur in scripts, argumenten | Tekstbestanden inlezen en analyseren |
| training 5 april | Exploratieve data analyse; verbanden tussen variabelen | Exploratieve data analyse uitvoeren op eigen dataset |
| training 19 april | Zelfstandige opdracht exploratieve dataanalyse | Evaluatie |
