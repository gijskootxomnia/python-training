# Python training

## Doelgroep

Professionals met affiniteit met het werken met data en computers. De bedoeling is dat de training wordt gevolgd met een laptop die de cursist zelf meeneemt, en cursisten wordt aangeraden Anaconda te gebruiken om een volledig toegeruste Python installatie te verkrijgen. Er is geen officieel huiswerk, maar om het tempo bij te houden is het verstandig om de opdrachten die tijdens de dagdelen voorbij komen nog eens goed door te nemen voor de volgende les.

## Leerdoelen

Zelfstandig met python kunnen werken,

* Scripts schrijven om taken te automatiseren, bijvoorbeeld een script dat csv- of excel-bestanden inleest, daar een berekening op doet en de resultaten wegschrijft naar een ander bestand.
* Een exploratieve dataanalyse uitvoeren met behulp van een Jupyter-notebook.

Er is gekozen om met Python aan de slag te gaan vanwege het relatief lage instapniveau en de flexibiliteit. Daarnaast is Python een populaire taal voor data-analyse, en zijn er externe packages beschikbaar van hoge kwaliteit, die allerlei taken eenvoudig maken. In het bijzonder komen de packages Pandas, Numpy, en het gebruik van het interactieve Jupyter notebook aan bod.

## Opzet

De training bestaat uit zes dagdelen van ongeveer 4 uur. Deze blokken worden opgedeeld met twee pauzes, en bestaan voor ongeveer twee derde uit centrale uitleg en het beantwoorden van vragen door de trainer, en daarnaast werken cursisten alleen of in tweetallen aan praktische programmeeropdrachten.

### Dagdeel 1: Getting started

We beginnen de training met de basis en een overzicht. We   bespreken wat de verschillende manieren zijn om Python te gebruiken. In kleine opdrachten wordt gekeken naar de flow van python scripts, de ingebouwde datastructuren en het gebruik van het Jupyter-notebook.

### Dagdeel 2: Werken met Python

Na de eerste kennismaking kijken we naar de functies en mogelijkheden die Python een productieve programmeertaal maken. We duiken verder in beschikbare datatypes, en ontdekken de standaard collectie van tools die met elke `python` installatie gebundeld zijn.

### Dagdeel 3: Scripts en libraries

In dit dagdeel doen we enige ervaring op met het schrijven van scripts. In vergelijking met het gebruik van het notebook komt er wat meer kijken bij het uitvoeren en schrijven van scripts. Daarnaast wordt het nog belangrijker de structuur en flow van Python te begrijpen. Ook komt het (her)gebruik van bestaande scripts aan bod.

### Dagdeel 4: Dataanalyse met Python (1)

In het vierde deel van de training gaan we aan de slag met de meest gebruikte tools voor het werken met data in Python. Numpy is hierbij het fundament voor het werken met getallen en `scipy` is een samenstelling van fundamentele statistische en wiskundige tools.

### Dagdeel 5: Dataanalyse met Python (2)

Pandas bouwt voort op Numpy en biedt functionaliteit die vergelijkbaar is met die van Excel; een tabelstructuur en eenvoudige visualisatietools. In dit dagdeel gebruiken we pandas voor het inlezen, opschonen en visualiseren van data, op basis van een aantal voorbeeld-datasets.

### Dagdeel 6: Automatiseren met Python

Het laatste dagdeel van de training richt zich op de buitenwereld. We behandelen hoe je data kunt verzamelen van andere websites en databases, waarbij we de database `sqlite3` gebruiken. Daarnaast gaan we dieper in op het maken van scripts, en het samenwerken met anderen met git en GitHub.

### Other Resources

### Resources

* Reading and watching
    * PyMC https://www.youtube.com/watch?v=Ajmj5itd2s8
    * PyData https://www.youtube.com/channel/UCOjD18EJYcsBog4IozkF_7w
    * https://learnxinyminutes.com/docs/python3/
    * http://stanfordpython.com/#lecture
    * https://www.youtube.com/watch?v=VVbJ4jEoOfU

### What's not part of this

* Object oriented programming
* `itertools`, `permutations`, `regex`, `yield`
* Bokeh visualizations

### Details

## 1 Introduction

* What is Python
    * Comparison with other programming languages
        * Why is Python popular
    * How to learn Python?
        * Book
        * Reddit
        * Build things
        * How this course is organized
            * Schedule
            * Assignments
            * Contact
    * Differences between 2.7 en 3.5
        * https://eev.ee/blog/2016/07/31/python-faq-why-should-i-use-python-3/
* Running python
    * Oneliners
    * Scripts
    * The notebook
        * Tab completion and getting help
    * Working directory
    * Reading and writing files
        * http://www.grouplens.org/node/73
* Basics in Python
    * Assignment and printing
    * Datatypes and operators
        * bool, list, int, float
        * `in`, `or`, ==, `and`, `is`
        * `*` and `plus`
    * Program flows
        * For loops
            * The `range` function
            * Using `list` for looping
        * If statements
        * Functie statement
            * Return values
            * Side-effects

## 2. Using Python

* Refreshing and practice with the basics
    * `for` loops and `def` statements
* More data structures: `dict`, `tuple`, `set`
    * Understand their structure
    * Looping over a dictionary
* Iterators, lists and generators
    * A closer look at the `range` function
    * List comprehensions
* Builtin functions
    * enumerate
    * zip
    * sort vs. sorted
    * map
* `stdlib` highlights
    * https://docs.python.org/3/reference/index.html#reference-index
    * https://docs.python.org/3/library/
    * datetime.datetime
        * Exercise Converting timestamps in moviedatabase
        * UserID::MovieID::Rating::Timestamp
    * string

## 3 scripts

* Python scripts, command-line tools en editors
* Writing basic scripts
    * Structuur
    * `if __name__=="__main__"`
* New `python` topics
    * Globals en locals, assignment and scopes
    * The `logging` library
* Veelvoorkomende third-party packages en hun functies
    * `import`
    * installing packages
    * `requests`, `csv`, `json`
* Hergebruik van scripts en modules


## 4 numpy, scipy en matplotlib

* Werken met de datatypen en functies in `numpy`
* Simulaties en berekeningen met `scipy.stats`
* Het genereren en opslaan van visualisaties met `matplotlib`
* Demonstratie van packages die hierop doorbouwen, bijvoorbeeld `statsmodels` en `seaborn`

* classes (overview, geen hands-on)
* `numpy` and `scipy`
    * http://www.scipy-lectures.org/intro/numpy/index.html
    * https://galeascience.wordpress.com/2016/08/10/top-10-pandas-numpy-and-scipy-functions-on-github/
    * Matrices and arrays
    * Pointwise functions
    * Datatypes
    * Adding and substracting, broadcasting rules
    * Indexing
    * `scipy`
        * `stats`
* `matplotlib`
    * Scatterplots, histograms and lineplots
* Widgets in Jupyter-notebook with interact

## 5 pandas

* What is pandas
    * Reading files
    * Series and Dataframes
* Data omvormen en opslaan
* Visualisaties met Pandas
* Pandas in action
    * Case: RDW
        * Column datatypes
        * `to_numeric` & `to_datetime`
        * Adding, subtracting, and mapping series
        * Column datatypes: str en dt
        * Datetime indices
        * Groupby
        * Apply
* Visualization and exploration
    * Boxplots
    * Scatterplots


## 6 Working with Python

* Git
    * Repositories and code sharing
* External data
    * HTML, HTTP en .json
    * Using requests to get or upload data
    * Storing data with pandas and sqllite
