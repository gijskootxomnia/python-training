# Voorbereiding _Becoming a Data Driven Analyst_ : Python I

_17 mei_

## Installeren

* Anaconda https://www.continuum.io/downloads. Kies 64 bit, python 3.6, volledige installatie.

Voor instructies, volg https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/install.html

## Test installatie

1. Maak een bestand met de tekst `import numpy; print("hello")` (1 regel)
2. Sla dat bestand op als `test.py` in een map op je desktop
3. Open Anaconda Prompt
4. Navigeer naar de juiste map en run het script met `python test.py`
5. Check of de output klopt en je geen foutmeldingen krijgt

## Voorbereiding

* Lees https://docs.python.org/3.6/tutorial/introduction.html

* Schrijf een script dat de eerste 100 fibonnaci getallen print. Run het zoals je eerder `test.py` hebt gerund.

* Bekijk de uitleg op http://nbviewer.jupyter.org/github/jupyter/notebook/blob/master/docs/source/examples/Notebook/Notebook%20Basics.ipynb

* Maak een nieuw jupyter-notebook waarin je de 100 eerste fibonacci getallen uitrekent en als output laat zien
