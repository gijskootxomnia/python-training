## Day 2 

### Pandas

* Read .csv and .xlsx files with pandas
* Selecting rows and columns
	* .loc, bracket notation
* Basic statistics and value counts
* Plotting series and dataframes: scatterplots, histograms and lineplots

### Power to the panda

* Transformations
* Groupby operations
* Working with dates and strings

### Moar power

* Predictions demonstration
